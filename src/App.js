import React, { Component } from 'react';
import jsonData from './jsonData.json';
import './main.css';


class ListItem extends Component {
  state = {
    checked: true
  }
  checked = (event) => {
    console.log(this.state);
    let newChecked = this.state.checked === false ? true : false;
    this.setState({ checked: newChecked});
    console.log(newChecked);
  }
  render (){
    return (
      <div className={this.state.checked === true ? "myList__Item" : "myList__Item done"}>
        <div> {this.props.item.name } </div>
        <div><button onClick={this.checked}>checked</button> </div>
      </div>
    )}
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: jsonData
    };
   
  };

  handleChange = (event) => {
    let searchQuery = event.target.value.toLowerCase();
    let renderContacts = jsonData.filter( (item) => {
      let itemName = item.name.toLowerCase();
      return itemName.indexOf(searchQuery) !== -1;
    });
    this.setState({ contacts: renderContacts});
  };

  render() {
    let {contacts} = this.state;
    return (
      <div className="App">
      
        <h1>My list app</h1>
        <input onChange={this.handleChange}/>
        {
          contacts.map( (item, key) =>{
            return ( 
            <ListItem key={key} item={item}/>);
          })
        }
      </div>
    );
  }
}

export default App;
